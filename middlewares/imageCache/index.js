module.exports = (strapi) => {
  return {
    initialize() {
      strapi.app.use(async (ctx, next) => {
        await next();

        const cachedImageExtensions = [
          "image/jpeg",
          "jpeg",
          "image/png",
          "png",
        ];
        if (cachedImageExtensions.indexOf(ctx.type) != -1)
          ctx.set("Cache-Control", 365 * 24 * 60 * 60);
      });
    },
  };
};

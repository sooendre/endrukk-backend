"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#life-cycle-callbacks)
 * to customize this model
 */
const Utilities = require("../../../classes/Utilities");

const generateSlug = async (attr) => {
  return Utilities.slugify("/" + attr.title);
};

module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      if (!data.slug) data.slug = await generateSlug(data);
    },
    beforeUpdate: async (model, attr, options) => {
      console.log(!attr.slug, attr.slug);
      if (!attr.slug) attr.slug = await generateSlug(attr);
    },
  },
};

"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#life-cycle-callbacks)
 * to customize this model
 */
const Utilities = require("../../../classes/Utilities");

module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      if (!data.slug) data.slug = Utilities.slugify(data.Name);
    },
    beforeUpdate: async (model, attr, options) => {
      if (!attr.slug) attr.slug = Utilities.slugify(attr.Name);
    },
  },
};

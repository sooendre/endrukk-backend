module.exports = () => ({
  settings: {
    imageCache: {
      enabled: true,
    },
    poweredBy: {
      enabled: false,
    },
  },
});
